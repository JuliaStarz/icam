$(function() {
	$('.features, .land-menu').on('click', 'a', function(e) {
		e.preventDefault();
		var name = $(this).attr('href');
		$('.features a, .land-menu a').removeClass('active');
		$(this).addClass('active');
		$(name).siblings('div').slideUp();
		$(name).slideDown();
	});
	$('.features a:first, .land-menu a:first').click();
	
	$('.catalog-view').on('click', 'a', function(e) {
		e.preventDefault();
		$('.catalog-view a').removeClass('active');
		$(this).addClass('active');
		$('.catalog-view a').each(function() {
			var name = $(this).attr('href');
			if($(this).hasClass('active'))
				$('.catalog_list-items').addClass(name);
			else
				$('.catalog_list-items').removeClass(name);
		});
		
	});
});